<%@ Import Namespace="System.Data.Odbc" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="Utility" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Security.Cryptography" %>

<%@ Page Language="c#" %>

<html>
<head>
    <title>Download Manager</title>
    <meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
    <link rel="stylesheet" type="text/css" href="styles.css">
    <link rel="stylesheet" type="text/css" href="css/custom-theme/jquery-ui-1.8.10.custom.css">
    <script language="javascript" src="js/jquery-1.4.4.min.js"> </script>
    <script language="javascript" src="js/jquery-ui-1.8.9.custom.min.js"> </script>
    <style type="text/css">
        .style1
        {
            font-size: 12px;
        }
        .style2
        {
            color: #696969;
        }
        .style3
        {
            font-size: xx-small;
        }
        .style15
        {
            color: #666666;
            font-size: 12px;
        }
        .style16
        {
            font-size: 10px;
        }
        A:link
        {
            font-size: xx-small;
        }
        INPUT
        {
            font-size: xx-small;
        }
    </style>
    <script runat="server">

        private String orderby = "";
        private String filter = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            if ((!context.Request.UserHostAddress.StartsWith("192.168.") && !context.Request.UserHostAddress.StartsWith("10.10.")) && context.Request.ServerVariables["SERVER_NAME"] != "localhost" && context.Request.ServerVariables["SERVER_NAME"] != "127.0.0.1")
            {
                //context.Response.Write(context.Request.ServerVariables["SERVER_NAME"]);
                context.Response.Write("<span style='position:absolute; top:45%; left:45%;'<h3>This application is not available outside of Solomon Associates.</h3>");
                context.Response.End();
            }

            if (!IsPostBack)
            {
                BuildOrder();
                BindData();
            }

            if (!DataGrid2.Visible)
                DataGrid2.Visible = true;

            ltDialog.Text = "";
            lblKey.Text = "";
            lblKey.Visible = false;
        }

        protected void DataGrid2_EditCommand(object source, DataGridCommandEventArgs e)
        {
            DataGrid2.EditItemIndex = e.Item.ItemIndex;
            BuildOrder();
            BuildFilter();
            BindData();
        }

        protected void DataGrid2_Cancel(object source, DataGridCommandEventArgs e)
        {
            DataGrid2.EditItemIndex = -1;
            BuildOrder();
            BuildFilter();
            BindData();
        }

        protected void DataGrid2_UpdateCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                String failedLogins = ((TextBox)e.Item.Cells[7].Controls[0]).Text;
                String makePermanent = ((TextBox)e.Item.Cells[6].Controls[0]).Text;
                String email = ((TextBox)e.Item.Cells[4].Controls[0]).Text;
                String file = ((TextBox)e.Item.Cells[5].Controls[0]).Text;
                String key = e.Item.Cells[3].Text.Trim();//id = mygrid.DataKeys[mygrid.EditItemIndex];
                String updateQry = "UPDATE FileKey SET email='" + email + "',filepath='" + file + "' , LoginAttempts=" + failedLogins + ", DisabledAutoDelete=" + makePermanent + " WHERE filekey='" + key + "'";

                string CONNSTR = string.Empty;
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["isDev"]))
                    CONNSTR = ConfigurationManager.ConnectionStrings["DownloadManagerDEV"].ToString();
                else
                    CONNSTR = ConfigurationManager.ConnectionStrings["DownloadManagerPROD"].ToString();

                using (OdbcConnection conn = new OdbcConnection(CONNSTR))
                {
                    conn.Open();

                    using (OdbcCommand updateCmd = new OdbcCommand(updateQry, conn))
                    {
                        updateCmd.ExecuteNonQuery();
                        DataGrid2.EditItemIndex = -1;
                        BuildOrder();
                        BuildFilter();
                        BindData(conn);
                    }
                }

            }
            catch (System.Exception ex)   // file IO errors
            {
                Response.Write(ex.Message);
            }
            finally
            {
                //conn.Close();
            }
        }

        protected void DataGrid2_SortCommad(Object source, DataGridSortCommandEventArgs e)
        {
            String columnName = e.SortExpression;

            if (e.SortExpression == "Posted")
                columnName = "PostedDate";

            if (e.SortExpression == "Downloaded")
                columnName = "DownloadDate";


            if (Session["sortBy"] != columnName)
                Session["sortBy"] = "[" + columnName + "]";

            // else
            // {
            if (Session["DescAsc"] == " desc ")
                Session["DescAsc"] = " asc ";
            else
                Session["DescAsc"] = " desc ";
            // }
            //orderby =" order by " + columnName;

            BuildOrder();
            BuildFilter();
            BindData();
        }

        protected void DataGrid2_DeleteCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                String delQry = "DELETE from FileKey Where filekey='" + e.Item.Cells[3].Text.Trim() + "'";
                String[] tokens = e.Item.Cells[5].Text.Split("\\".ToCharArray());
                String filename = tokens[tokens.Length - 1];

                if (File.Exists("D:\\dlm_ftproot\\" + filename))
                {
                    File.Delete("D:\\dlm_ftproot\\" + filename);

                    lblKey.Visible = true;
                    lblKey.Text = filename + " was sucessfully deleted ";
                    ltDialog.Text = " $('#commentDialog').dialog({    modal:false, " +
                                        "      height:200," +
                                        "      width:400, " +
                                        "      buttons:{  " +
                                        "               'cancel' : {text:'OK',className:'cancel',click: function(){$(this).dialog('close');}} " +
                                        "              } " +
                                        "  });";
                }

                string CONNSTR = string.Empty;
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["isDev"]))
                    CONNSTR = ConfigurationManager.ConnectionStrings["DownloadManagerDEV"].ToString();
                else
                    CONNSTR = ConfigurationManager.ConnectionStrings["DownloadManagerPROD"].ToString();
                
                using (OdbcConnection conn = new OdbcConnection(CONNSTR))
                {
                    conn.Open();

                    OdbcCommand delCmd = new OdbcCommand(delQry, conn);
                    delCmd.ExecuteNonQuery();

                    BuildOrder();
                    BuildFilter();
                    BindData(conn);
                }

            }
            catch (System.Exception ex)   // file IO errors
            {
                Response.Write(ex.Message);
            }
        }

        protected void DataGrid2_OnGridItemCreated(object sender, DataGridItemEventArgs e)
        {
            //e.Item.Cells[2].Visible = false;

        }

        protected void DataGrid2_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            DataGrid2.CurrentPageIndex = e.NewPageIndex;
            BuildOrder();
            BuildFilter();
            BindData();
        }


        protected void DataGrid2_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Let empty
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            BuildOrder();
            BuildFilter();
            BindData();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            ClearFilter();
            BuildOrder();
            BindData();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            String strScript = "";
            String message = "";
            Boolean err = false;

            try
            {
                if (txtEmail.Text == "")
                {
                    message = "Please enter an email";
                    err = true;
                }

                if (txtPassword.Text == "")
                {
                    message = "Please enter a password";
                    err = true;
                }

                if (txtPasswordAgain.Text == "")
                {
                    message = "Please enter the password again";
                    err = true;
                }


                if (txtFile.PostedFile.FileName == "")
                {
                    message = "Please enter a file name";
                    err = true;
                }

                if (txtMaxDownload.Text == "")
                {
                    message = "Please enter the maximum number of downloads";
                    err = true;
                }

                if (txtPassword.Text != txtPasswordAgain.Text)
                {
                    message = "The passwords does not match. Please enter passwords again.";
                    err = true;
                }

                if (!err)
                {
                    SubmitData();
                    UploadFile();
                    //lblKey.Visible = true;

                }
                else
                {

                    lblKey.Text = message;
                    lblKey.Visible = true;
                    ltDialog.Text = " $('#commentDialog').dialog({    modal:false, " +
                                        "      height:225," +
                                        "      width:400, " +
                                        "      buttons:{  " +
                                        "               'cancel' : {text:'OK',className:'cancel',click: function(){$(this).dialog('close');}} " +
                                        "              } " +
                                        "  });";

                }
            }
            catch (System.Exception ex)   // file IO errors
            {
                Response.Write(ex.Message);
            }
        }

        protected void Item_Bound(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                DataView dv = (DataView)DataGrid2.DataSource;
                DataColumnCollection dc = dv.Table.Columns;
                int index = dc.IndexOf(dc["Key"]) + 3;
                e.Item.Cells[index].Visible = false;


                if (e.Item.Cells[0].Controls.Count > 0)
                {
                    LinkButton linkBtn = (LinkButton)e.Item.Cells[0].Controls[0];
                    linkBtn.Attributes["onclick"] += "return confirm('Are you sure you wish to delete these records');";
                }
            }
            catch (System.Exception ex)   // file IO errors
            {
                throw ex;
            }
        }



        private void SubmitData()
        {
            //String Url =  Request.ServerVariables["PATH_INFO"];
            String appPath = Request.ServerVariables["PATH_INFO"].Split('/')[1];
            String genKey;
            String cmd;
            String email;

            //FileInfo fInfo = new FileInfo(txtFile.PostedFile.FileName);
            String newFileName;
            String oldFileName = System.IO.Path.GetFileName(txtFile.PostedFile.FileName);

            try
            {
                lblKey.Text = "";

                newFileName = CheckFilename(oldFileName);

                string CONNSTR = string.Empty;
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["isDev"]))
                    CONNSTR = ConfigurationManager.ConnectionStrings["DownloadManagerDEV"].ToString();
                else
                    CONNSTR = ConfigurationManager.ConnectionStrings["DownloadManagerPROD"].ToString();
                
                using (OdbcConnection conn = new OdbcConnection(CONNSTR))
                {
                    conn.Open();
                    genKey = GenerateKey(conn);

                    lblKey.Text = "The following link  is for downloading the file, " + oldFileName + ": <br><br>";
                    lblKey.Text += "http://webservices.solomononline.com/" + appPath + "/Authenticate.aspx?key=" + genKey + "<br>";

                    if (RadioButtonList1.SelectedItem.Value == "false")
                        lblKey.Text += "This link will be enabled for 30 days.<br><br>";
                    else
                        lblKey.Text += " <br><br>";

                    lblKey.Text += "Your password will be sent to you in a separate email.<br>";

                    email = "'mailto:" + txtEmail.Text + "?subject=" +
                               Server.UrlEncode(oldFileName) +
                               "%20is%20available%20to%20download&Body=" +
                               lblKey.Text.Replace("<br>", "%0A%0D") + "'";

                    cmd = "Insert into FileKey(FileKey,FilePath,Email,Password,MaxDownloadCount,PostedDate,DisabledAutoDelete,DownloadCount,PostedBy,ClientID,ActualFileName) " +
                            "Values('" +
                            genKey + "','" +
                            "D:\\dlm_ftproot\\" + newFileName + "','" +
                            txtEmail.Text + "','" +
                            EncryptText(txtPassword.Text) + "'," +
                            Single.Parse(txtMaxDownload.Text.Trim()) + ",'" +
                            DateTime.Now.ToString() + "'," +
                            Convert.ToByte(Convert.ToBoolean(RadioButtonList1.SelectedItem.Value)) + ",0,'" +
                            txtPoster.Text + "','" +
                            txtClient.Text + "','" +
                            oldFileName + "')";

                    using (OdbcCommand iCommand = new OdbcCommand(cmd, conn))
                    {

                        // Execute the SQL command
                        int nNoAdded = iCommand.ExecuteNonQuery();
                        //DataGrid2.Visible = false;
                        lblKey.Visible = true;
                        BuildOrder();
                        BuildFilter();
                        BindData(conn);
                    }
                }

                ltDialog.Text = " $('#commentDialog').dialog({    modal:false, " +
                                     "      height:300," +
                                     "      width:700, " +
                                     "      buttons:{  " +
                                     "               'save' : {text:'Send to Outlook',click: function(){ window.location.href=" + email + " ;$(this).dialog('close');}}," +
                                     "               'cancel' : {text:'OK',className:'cancel',click: function(){$(this).dialog('close');}} " +
                                     "              } " +
                                     "  });";

                //if (!Convert.ToBoolean(ConfigurationManager.AppSettings["isDev"]))
                //    Logger.LoggedEntry("UPLOAD INFO -" + oldFileName + " was uploaded on the server for " + txtEmail.Text);
            }
            catch (System.Exception ex)   // file IO errors
            {
                throw ex;
            }
        }

        private String GenerateKey(OdbcConnection conn)
        {
            OdbcDataReader reader = null;
            OdbcCommand cCommand = new OdbcCommand("select Count(*) from FileKey", conn);
            MD5CryptoServiceProvider encrypter = new MD5CryptoServiceProvider();
            Byte[] encryptedKey;
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

            try
            {
                reader = cCommand.ExecuteReader();
                reader.Read();
                encryptedKey = encrypter.ComputeHash(encoder.GetBytes(txtEmail.Text + reader.GetInt32(0) + DateTime.Now.ToShortTimeString()));
                reader.Close();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return HexEncoding.ToString(encryptedKey);
        }

        private void BindData()
        {
            String genKey;
            String cmd = null;
            //String sltQry = "select FileKey as [Key],Email,FilePath as [File],CAST(DisabledAutoDelete as int) as [Post Permanently], LoginAttempts as [Failed Logins],PostedDate as [Posted], DownloadDate as [Downloaded] from FileKey " + filter + orderby;
            String sltQry = "select FileKey as [Key],Email,FilePath as [File],CAST(DisabledAutoDelete as int) as [Post Permanently], LoginAttempts as [Failed Logins],PostedDate as [Posted],ActualFileName from FileKey " + filter + orderby;
            //Response.Write(sltQry);
            try
            {
                string CONNSTR = string.Empty;
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["isDev"]))
                    CONNSTR = ConfigurationManager.ConnectionStrings["DownloadManagerDEV"].ToString();
                else
                    CONNSTR = ConfigurationManager.ConnectionStrings["DownloadManagerPROD"].ToString();
                
                using (OdbcConnection conn = new OdbcConnection(CONNSTR))
                {
                    conn.Open();

                    // Response.Write(sltQry);
                    using (OdbcDataAdapter odbcAdapter = new OdbcDataAdapter(sltQry, conn))
                    {
                        DataSet datSet = new DataSet();

                        odbcAdapter.Fill(datSet);

                        datSet.Tables[0].Columns["Key"].ReadOnly = true; //Prevents the key field from being updated
                        datSet.Tables[0].Columns["Posted"].ReadOnly = true;//Prevents the date field from being updated
                        //datSet.Tables[0].Columns["Downloaded"].ReadOnly=true;  //Prevents the date field from being updated


                        DataGrid2.DataSource = datSet.Tables[0].DefaultView;
                        DataGrid2.VirtualItemCount = datSet.Tables[0].Rows.Count;
                        DataGrid2.ItemStyle.Wrap = false;
                        DataGrid2.DataKeyField = "Key";

                        DataGrid2.DataBind();
                    }

                }

            }
            catch (System.Exception ex)   // file IO errors
            {
                Response.Write(ex.Message);
            }

        }

        private void BindData(OdbcConnection conn)
        {
            String genKey;
            String cmd = null;
            //String sltQry = "select  FileKey as [Key],Email,FilePath as [File],CAST(DisabledAutoDelete as int) as [Post Permanently], LoginAttempts as [Failed Logins],PostedDate as [Posted], DownloadDate as [Downloaded] from FileKey" + filter + orderby;
            String sltQry = "select  FileKey as [Key],Email,FilePath as [File],CAST(DisabledAutoDelete as int) as [Post Permanently], LoginAttempts as [Failed Logins],PostedDate as [Posted],ActualFileName from FileKey" + filter + orderby;
            // Response.Write(sltQry);
            try
            {
                OdbcDataAdapter odbcAdapter = new OdbcDataAdapter(sltQry, conn);
                DataSet datSet = new DataSet();

                datSet.Clear();
                odbcAdapter.Fill(datSet);

                datSet.Tables[0].Columns["Key"].ReadOnly = true; //Prevents the key field from being updated
                datSet.Tables[0].Columns["Posted"].ReadOnly = true;//Prevents the date field from being updated
                //datSet.Tables[0].Columns["Downloaded"].ReadOnly=true;//Prevents the date field from being updated


                DataGrid2.DataSource = datSet.Tables[0].DefaultView;
                DataGrid2.VirtualItemCount = datSet.Tables[0].Rows.Count;
                DataGrid2.ItemStyle.Wrap = false;
                DataGrid2.DataBind();

            }
            catch (System.Exception ex)   // file IO errors
            {
                Response.Write(ex.StackTrace);
            }
        }

        private void BuildOrder()
        {
            if (Session["sortBy"] == null)
                Session["sortBy"] = "PostedDate";

            if (Session["DescAsc"] == null)
                Session["DescAsc"] = " desc ";

            orderby = " order by " + Session["sortBy"] + Session["DescAsc"];
        }

        private void ClearFilter()
        {
            txtKeywordFilter.Text = "";
            txtPosterFilter.Text = "";
            txtEmailFilter.Text = "";
            txtFilenameFilter.Text = "";
            txtClientFilter.Text = "";
            dlTimespanFilter.SelectedIndex = -1;
        }

        private void BuildFilter()
        {
            filter = " WHERE ";

            if (txtKeywordFilter.Text != String.Empty)
                filter += " GroupBy like '%" + txtKeywordFilter.Text + "%'  AND ";

            if (txtPosterFilter.Text != String.Empty)
                filter += " PostedBy like '%" + txtPosterFilter.Text + "%'  AND ";

            if (txtEmailFilter.Text != String.Empty)
                filter += " EMail like '%" + txtEmailFilter.Text + "%'  AND ";


            if (txtFilenameFilter.Text != String.Empty)
                filter += " FilePath like '%" + txtFilenameFilter.Text + "%'  AND ";


            if (txtClientFilter.Text != String.Empty)
                filter += " ClientID like '%" + txtClientFilter.Text + "%'  AND ";

            if (dlTimespanFilter.SelectedValue != String.Empty)
            {
                switch (dlTimespanFilter.SelectedValue)
                {
                    case ".25":
                        filter += " PostedDate > '" + System.DateTime.Now.AddDays(-7) + "'  AND ";
                        break;
                    case ".50":
                        filter += " PostedDate > '" + System.DateTime.Now.AddDays(-14) + "'  AND ";
                        break;
                    default:
                        filter += " PostedDate > '" + System.DateTime.Now.AddMonths(-1 * Convert.ToInt16(dlTimespanFilter.SelectedValue)) + "'  AND ";
                        break;
                }
            }

            //if (txtBeginDateFilter.Text !=  String.Empty && txtEndDateFilter.Text !=  String.Empty )
            //   filter += " PostedDate BETWEEN '" + txtBeginDateFilter.Text + "' AND '" + txtEndDateFilter.Text + "' ";

            if (filter.EndsWith("AND "))
                filter = filter.Substring(0, filter.Length - 4);

            if (filter == " WHERE ")
                filter = "";

            // Response.Write(filter);
        }

        private String EncryptText(String msg)
        {
            MD5CryptoServiceProvider encrypter = new MD5CryptoServiceProvider();
            Byte[] encryptedKey;
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

            try
            {
                encryptedKey = encrypter.ComputeHash(encoder.GetBytes(msg));
                return HexEncoding.ToString(encryptedKey);

            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        private void UploadFile()
        {
            try
            {
                FileInfo fInfo = new FileInfo(txtFile.PostedFile.FileName);
                String fileName = System.IO.Path.GetFileName(txtFile.PostedFile.FileName);

                fileName = CheckFilename(fileName);

                if (txtFile.PostedFile != null)
                {
                    if (txtFile.PostedFile.ContentLength <= 0)
                    {

                        lblKey.Text = " The upload failed because the file size is zero";
                        ltDialog.Text = " $('#commentDialog').dialog({    modal:false, " +
                                        "      height:200," +
                                        "      width:400, " +
                                        "      buttons:{  " +
                                        "               'cancel' : {text:'OK',className:'cancel',click: function(){$(this).dialog('close');}} " +
                                        "              } " +
                                        "  });";
                    }
                    else
                    {
                        //Save the file with the same name under the folder where the aspx file exists
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["isDev"]))
                            txtFile.PostedFile.SaveAs("c:\\temp\\dlm\\" + fileName);
                        else
                            txtFile.PostedFile.SaveAs("D:\\dlm_ftproot\\" + fileName);
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        private String CheckFilename(String fileName)
        {
            String tmpFileName;
            Int32 i = 0;

            tmpFileName = fileName;
            while (File.Exists("D:\\dlm_ftproot\\" + fileName))
            {
                tmpFileName = i + "_" + fileName;

                if (!File.Exists("D:\\dlm_ftproot\\" + tmpFileName))
                {
                    fileName = tmpFileName;
                }
                i += 1;
            }

            return fileName;
        }

    </script>
</head>
<body style="background: url(images/DownloadManager_Console.jpg) no-repeat" leftmargin="0"
    topmargin="0" marginheight="0" marginwidth="0">
    <!-- ImageReady Slices (template.psd) -->
    <form id="Form1" enctype="multipart/form-data" runat="server">
    <table id="Table_01" border="0" cellspacing="0" cellpadding="5" width="900" height="662">
        <tr>
            <td height="69" rowspan="2" width="391" colspan="2">
                &nbsp;
            </td>
            <td height="15">
            </td>
        </tr>
        <tr>
            <td height="22" align="right">
                <a href="http://webservices.solomononline.com/dlm/event.html" target="_blank"><font
                    size="1">View Log</font></a>
            </td>
        </tr>
        <tr valign="top">
            <td height="100%" width="425" colspan="2" align="right">
                <p class="style3" align="left">
                    <font color="#696969"></font>&nbsp;</p>
            </td>
            <td height="100%">
                <!-- My Content -->
                <table border="0" cellspacing="0" cellpadding="0" width="538" align="right" height="424">
                    <tbody>
                        <tr>
                            <td height="218" width="687" colspan="2">
                                <p class="style16" align="left">
                                    <table style="height: 142px" border="0" cellspacing="0" cellpadding="0" width="91%"
                                        align="center" height="168">
                                        <tbody>
                                            <tr>
                                                <td height="23" width="156">
                                                    <span class="style16">Email </span>
                                                </td>
                                                <td height="23" valign="middle" width="377">
                                                    <span class="style1"><strong>
                                                        <asp:TextBox ID="txtEmail" runat="server" Font-Size="XX-Small" BorderColor="Maroon"
                                                            BorderWidth="1px" BorderStyle="Solid"></asp:TextBox><font color="#000000" size="1"></font></strong></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="23" width="156">
                                                    <span class="style16">File </span>
                                                </td>
                                                <td width="377">
                                                    <span class="style1"><strong>
                                                        <input style="border-bottom: maroon 1px solid; border-left: maroon 1px solid; border-top: maroon 1px solid;
                                                            border-right: maroon 1px solid" id="txtFile" class="SmallFont" size="40" type="file"
                                                            name="txtFile" runat="server" font-size="XX-Small">
                                                    </strong></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="156">
                                                    <span class="style16">Password </span>
                                                </td>
                                                <td class="SmallFont" width="377">
                                                    <span class="style1"><strong>
                                                        <asp:TextBox ID="txtPassword" runat="server" Font-Size="XX-Small" BorderColor="Maroon"
                                                            BorderWidth="1px" BorderStyle="Solid" Width="117px" Columns="25" MaxLength="25"
                                                            TextMode="Password" Font-Names="Arial"></asp:TextBox></strong></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="17" width="156">
                                                    <font class="style16" face="Arial">Re-enter Password </font>
                                                </td>
                                                <td height="17" width="377">
                                                    <span class="style1">
                                                        <asp:TextBox ID="txtPasswordAgain" runat="server" Font-Size="XX-Small" BorderColor="Maroon"
                                                            BorderWidth="1px" BorderStyle="Solid" Width="117px" Columns="25" MaxLength="25"
                                                            TextMode="Password" Font-Names="Arial"></asp:TextBox></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="26" width="156">
                                                    <span class="style16">Number of Downloads&nbsp; </span>
                                                </td>
                                                <td width="377">
                                                    <span class="style1">
                                                        <asp:TextBox ID="txtMaxDownload" runat="server" Font-Size="XX-Small" BorderColor="Maroon"
                                                            BorderWidth="1px" BorderStyle="Solid" Columns="2" value="1"></asp:TextBox></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="156">
                                                    <span class="style16">Poster Name </span>
                                                </td>
                                                <td width="377">
                                                    <span class="style1">
                                                        <asp:TextBox ID="txtPoster" runat="server" Font-Size="XX-Small" BorderColor="Maroon"
                                                            BorderWidth="1px" BorderStyle="Solid" Width="117px" Columns="25" MaxLength="25"
                                                            TextMode="SingleLine" Font-Names="Arial"></asp:TextBox>(optional)</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="156">
                                                    <span class="style16">Client Name </span>
                                                </td>
                                                <td width="377">
                                                    <span class="style1">
                                                        <asp:TextBox ID="txtClient" runat="server" Font-Size="XX-Small" BorderColor="Maroon"
                                                            BorderWidth="1px" BorderStyle="Solid" Width="117px" Columns="25" MaxLength="25"
                                                            TextMode="SingleLine" Font-Names="Arial"></asp:TextBox>(optional)</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="156">
                                                    <span class="style16">Tags </span>
                                                </td>
                                                <td width="377">
                                                    <span class="style1">
                                                        <asp:TextBox ID="txtKeyword" runat="server" Font-Size="XX-Small" BorderColor="Maroon"
                                                            BorderWidth="1px" BorderStyle="Solid" Width="117px" Columns="45" MaxLength="45"
                                                            TextMode="SingleLine" Font-Names="Arial"></asp:TextBox>(optional)</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="156">
                                                    <span class="style16">Do you want to post the file post permanently?</span>
                                                </td>
                                                <td width="377">
                                                    <span class="style1">
                                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" Font-Size="XX-Small" BorderColor="Maroon"
                                                            Width="72px" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="true">Yes</asp:ListItem>
                                                            <asp:ListItem Value="false" Selected="True">No</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="center">
                                                    <span class="style1">
                                                        <asp:Button ID="Button1" OnClick="btnSubmit_Click" runat="server" Font-Size="12px"
                                                            BorderColor="Maroon" BorderWidth="1px" BorderStyle="Solid" Width="103px" Font-Names="Arial"
                                                            Height="21px" Text="Submit"></asp:Button></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!--My Content -->
            </td>
        </tr>
    </table>
    <span style="position: absolute; top: 300px; left: 200px" class="style1" align="center">
        <asp:DataGrid ID="DataGrid2" runat="server" Font-Size="8px" BorderColor="#CC9966"
            BorderWidth="1px" BorderStyle="Solid" Width="700px" Font-Names="Arial" EditItemStyle-Width="20px"
            EditItemStyle-Font-Size="8pt" OnItemDataBound="Item_Bound" OnUpdateCommand="DataGrid2_UpdateCommand"
            OnCancelCommand="DataGrid2_Cancel" OnEditCommand="DataGrid2_EditCommand" OnDeleteCommand="DataGrid2_DeleteCommand"
            CellPadding="2" BackColor="White" AllowPaging="True" AllowSorting="True" OnSelectedIndexChanged="DataGrid2_SelectedIndexChanged"
            OnPageIndexChanged="DataGrid2_PageIndexChanged" OnSortCommand="DataGrid2_SortCommad"
            GridLines="Horizontal" PageSize="10">
            <FooterStyle Wrap="False" ForeColor="#330099" BackColor="#FFFFCC"></FooterStyle>
            <SelectedItemStyle Font-Bold="True" Wrap="False" ForeColor="#663399" BackColor="#FFCC66">
            </SelectedItemStyle>
            <ItemStyle Font-Size="XX-Small" ForeColor="#330099" Width="30%" CssClass="ColItem"
                BackColor="White"></ItemStyle>
            <HeaderStyle Font-Size="XX-Small" Font-Bold="True" ForeColor="#FFFFCC" CssClass="ColItem"
                BackColor="#990000"></HeaderStyle>
            <Columns>
                <asp:ButtonColumn Text="&lt;IMG SRC=images/delete.jpg Border=0 Width=12 Height=12&gt;"
                    HeaderText="Delete" CommandName="Delete"></asp:ButtonColumn>
                <asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" HeaderText="Edit"
                    CancelText="Cancel" EditText="&lt;IMG SRC=images/Edit.gif Border=0 Width=12 Height=12&gt;">
                </asp:EditCommandColumn>
                <asp:TemplateColumn HeaderText="View" HeaderStyle-Width="15" ItemStyle-Width="25">
                    <ItemTemplate>
                        <a href="#" onclick="returnDownloadKey('<%# DataBinder.Eval(Container.DataItem, "Key").ToString().Trim() %>','<%# DataBinder.Eval(Container.DataItem, "Email").ToString().Trim() %>','<%# DataBinder.Eval(Container.DataItem, "ActualFileName").ToString().Trim() %>');">
                            <img src="images/icon-view.gif" border="0" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
            <PagerStyle VerticalAlign="Middle" HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"
                Wrap="False" Mode="NumericPages"></PagerStyle>
        </asp:DataGrid></span>
    <!-- End ImageReady Slices -->
    <div style="border-bottom: #330099 1px; position: absolute; border-left: #330099 1px;
        float: left; border-top: #330099 1px; top: 325px; border-right: #330099 1px;
        left: 10px; backgound: #330000" id="filering">
        <h6>
            Filter by</h6>
        <div>
            <span style="width: 50px; display: block" class="style16">Keyword(s) </span><span>
                <asp:TextBox ID="txtKeywordFilter" runat="server" Font-Size="XX-Small" BorderColor="Maroon"
                    BorderWidth="1px" BorderStyle="Solid"></asp:TextBox></span></div>
        <div>
            <span style="width: 50px; display: block" class="style16">Poster</span> <span>
                <asp:TextBox ID="txtPosterFilter" runat="server" Font-Size="XX-Small" BorderColor="Maroon"
                    BorderWidth="1px" BorderStyle="Solid"></asp:TextBox></span></div>
        <div>
            <span style="width: 50px; display: block" class="style16">Email </span><span>
                <asp:TextBox ID="txtEmailFilter" runat="server" Font-Size="XX-Small" BorderColor="Maroon"
                    BorderWidth="1px" BorderStyle="Solid"></asp:TextBox></span></div>
        <div>
            <span style="width: 50px; display: block" class="style16">Client </span><span>
                <asp:TextBox ID="txtClientFilter" runat="server" Font-Size="XX-Small" BorderColor="Maroon"
                    BorderWidth="1px" BorderStyle="Solid"></asp:TextBox></span></div>
        <div>
            <span style="width: 50px; display: block" class="style16">Filename </span><span>
                <asp:TextBox ID="txtFilenameFilter" runat="server" Font-Size="XX-Small" BorderColor="Maroon"
                    BorderWidth="1px" BorderStyle="Solid"></asp:TextBox></span></div>
        <div>
            <div class="style16">
                Posted Date Range</div>
            <span>
                <asp:DropDownList ID="dlTimespanFilter" runat="server" Font-Size="XX-Small" BorderColor="Maroon"
                    BorderWidth="1px" BorderStyle="Solid" Width="90">
                    <asp:ListItem Selected="True" Value="">  </asp:ListItem>
                    <asp:ListItem Value=".25"> 1 Week Ago </asp:ListItem>
                    <asp:ListItem Value=".50"> 2 Weeks Ago </asp:ListItem>
                    <asp:ListItem Value="1"> 1 Month Ago </asp:ListItem>
                    <asp:ListItem Value="3"> 3 Months Ago </asp:ListItem>
                    <asp:ListItem Value="12"> 1 Year Ago  </asp:ListItem>
                    <asp:ListItem Value="36"> 3 Years Ago </asp:ListItem>
                </asp:DropDownList>
            </span>
        </div>
        <div align="left">
            <br>
            <asp:Button ID="btnFilter" OnClick="btnFilter_Click" runat="server" Font-Size="12px"
                BorderColor="Maroon" BorderWidth="1px" BorderStyle="Solid" Width="50px" Font-Names="Arial"
                Height="21px" Text="Search"></asp:Button><asp:Button ID="btnFilterReset" OnClick="btnReset_Click"
                    runat="server" Font-Size="12px" BorderColor="Maroon" BorderWidth="1px" BorderStyle="Solid"
                    Width="50px" Font-Names="Arial" Height="21px" Text="Reset"></asp:Button></div>
    </div>
    </form>
    <div id="commentDialog" title="Download Details">
        <asp:Label ID="lblKey" runat="server"></asp:Label></div>
    <script language="javascript"> 
		<asp:Literal id="ltDialog" Runat=server></asp:Literal>

    </script>
    <script language="javascript">


        function returnDownloadKey(key, email, file) {
            var mailtoVar;
            var mesg;

            file = file.replace('D:dlm_ftproot', '');



            mesg = "The following link  is for downloading the file";
            if (jQuery.trim(file) != "")
                mesg = mesg + ", " + file;
            mesg = mesg + ": <br><br>";
            mesg = mesg + "http://webservices.solomononline.com/DLM/Authenticate.aspx?key=" + key;


            mailtoVar = "mailto:" + escape(email) + "?subject=" +
              escape(file) +
              "%20is%20available%20to%20download&Body=" +
               mesg.replace(/<br>/g, '%0A%0D');

            $("#commentDialog").html('');

            $("#commentDialog").append(mesg);

            $('#commentDialog').dialog({ modal: false,
                height: 300,
                width: 700,
                buttons: {
                    'save': { text: 'Send to Outlook', click: function () { window.location.href = mailtoVar; $(this).dialog('close'); } },
                    'cancel': { text: 'OK', className: 'cancel', click: function () { $(this).dialog('close'); } }
                }
            });
        }
    </script>
</body>
</html>
